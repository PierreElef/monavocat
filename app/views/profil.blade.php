@extends('mon_template')
@section('titre')
    Mon profil
@stop
@section('contenu1')
@parent
{{ Form::open(array('method'=>'post')) }}
<div class="form-group">
    <label>Login</label>
    {{Form::text('username', $user->username, ['class' => 'form-control'])}}
</div>
<div class="form-group">
    <label>Mail</label>
    {{Form::text('mail', $user->mail,['class' => 'form-control'])}}
</div>
<div class="form-group">
    <label>Password</label>
    {{Form::password('password' , array('class' => 'form-control'))}}
    </div>
{{ Form::submit('Mettre à jour', ['class' => 'btn abutton']) }}
{{ Form::close() }}
@if($user->role="admin")
    <a href={{url("admin")}}><button type="submit" class="btn abutton">Accès Admin</button></a>
@endif
@stop