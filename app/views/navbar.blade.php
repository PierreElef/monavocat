<div class="avonavbar">
    <ul class="nav justify-content-center">
    @foreach (Config::get('navbar.link') as $namePage=>$page)
        <li class="nav-item">
            <a class="nav-link" href={{url($page)}}>{{$namePage}}</a>
        </li>
    @endforeach
        @if (Auth::guest())
        <li class="nav-item">
            <a class="nav-link" href={{url('deconnexion')}} alt="Deconnexion">Se connecter</a>
        </li>
        @else
            <li class="nav-item">
                <a class="nav-link" href={{url('deconnexion')}} alt="Connexion">Se déconnecter</a>
            </li>
        @endif
        
    </ul>
</div>