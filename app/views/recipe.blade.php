@extends('mon_template')
@section('titre')
    {{$recipe->name}}
@stop
@section('contenu1')
@parent
    <div class="recipe row">
        <div class="col-md-3 d-none d-md-block border-bottom border-dark">
            @if ($recipe->image == '')
                <img src="{{asset('dist/img/download/avocado1_512.png')}}">
            @else
                <img src="{{asset('dist/img/download')}}/{{$recipe->image}}">
            @endif               
        </div>
        <div class="col-md-9 col-12 border-bottom border-dark">
            <h3>{{$recipe->name}}</h3> 
            <h4>{{$recipe->description}}</h4>
        </div>
        <div class="col-md-3 col-12 pt-2 pb-3 border-bottom border-dark">
            Type : {{$tag->name}}</br>
            @for($i=0; $i<$recipe_ing_measArray['size']; $i++)
                <strong>{{$recipe_ing_measArray['ing_'.$i]}}</strong> : {{$recipe_ing_measArray['amount_'.$i]}} {{$recipe_ing_measArray['meas_'.$i]}}</br>
            @endfor
        </div>
        <div class="col-md-9 col-12 pt-2 pb-3">
             {{str_replace("\r\n","<br/>",$recipe->instruction)}}
        </div>
    </div>
@stop