@extends('mon_template')
@section('titre')
    Ajouter une recette
@stop
@section('contenu1')
@parent
    <div class="addRecipe mt-1">
        <div class="text-center">Créez votre recette à base d&rsquo;avocat. Donnez le nom de votre recette, une petite description et le nombre d&rsquo;ingrédients</div>
        <div class="row justify-content-md-center">
            <div class="col-2 step">
                <img src={{ asset('/dist/img/icons/step1.png') }} height="100">
            </div>
            <div class="col-2 step">
                <img src={{ asset('/dist/img/icons/avocado5_512.png') }} height="100">
            </div>
            <div class="col-2 step">
                <img src={{ asset('/dist/img/icons/avocado5_512.png') }} height="100">
            </div>
        </div>
        <?php 
            $tagArray=[];
            foreach ($tags as $tag){
                $tagArray+=[$tag->id => $tag->name];
            }
        ?>
        {{ Form::open(array('method'=>'post', 'files' => true)) }}
        <div class="form-group">
            <label>Nom de la recette</label>
            {{Form::text('name', '', ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            <label>Description</label>
            {{Form::text('description', '',['placeholder'=>"Ex : pour combien de personne, type de plat,parfait pour...", 'class' => 'form-control'])}}
        </div>
        <div class="form-group">
            <label>Type de plat</label>
            {{Form::select('tag', $tagArray, null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            <label>Nombre d&rsquo;ingrédient</label>
            <input type="number" name="nbring" min="1" step="1" value="1" class="form-control recipeNumber">
        </div>
        {{ Form::submit('Suivant', ['class' => 'btn abutton']) }}
        {{ Form::close() }}
    </div>
@stop