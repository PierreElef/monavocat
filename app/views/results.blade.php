@extends('mon_template')
@section('titre')
    les Recettes
@stop
@section('contenu1')
@parent
    <div>
        @for($i=0; $i<$recipes['size']; $i++)
        <a href={{url('recipes')}}/{{$recipes['id_'.$i]}}>
            <div class="recipes row">
                <div class="col-md-2 d-none d-md-block">
                    @if ($recipes['image_'.$i] == '')
                        <img src="{{asset('dist/img/download/avocado1_512.png')}}">
                    @else
                        <img src="{{asset('dist/img/download')}}/{{$recipes['image_'.$i]}}">
                    @endif               
                </div>
                <div class="col-md-10 col-12">
                    <h3>{{$recipes['name_'.$i]}}</h3>
                    {{$recipes['description_'.$i]}}
                </div>
            </div>
        </a>
        @endfor
        
    </div>
@stop