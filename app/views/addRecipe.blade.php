@extends('mon_template')
@section('titre')
    Ajouter une recette
@stop
@section('contenu1')
@parent
    <div class="addRecipe">
        {{ Form::open(array('method'=>'post', 'files' => true)) }}
        <div class="form-group">
            <label>Nom de la recette</label>
            {{Form::text('name', '', ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            <label>Description</label>
            {{Form::text('description', '',['placeholder'=>"Ex : pour combien de personne, type de plat,parfait pour...", 'class' => 'form-control'])}}
        </div>
        <div class="form-group">
            <label>Image</label><br/>
            {{ Form::file('image') }}
        </div>
        <div class="form-group">
            <label>Instructions</label>
            {{Form::textarea('instruction', '',['placeholder'=>"Ecrire les instructions pour la recette", 'class' => 'addRecipeTextArea'])}}
        </div>
        {{ Form::submit('Ajouter', ['class' => 'btn abutton']) }}
        {{ Form::close() }}
    </div>
@stop