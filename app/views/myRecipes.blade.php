@extends('mon_template')
@section('titre')
    mes Recettes
@stop
@section('contenu1')
@parent
    <div class="mt-3">
        <div class="text-center"><h4>Voici la liste de toutes les recettes mis en ligne par vos soins</h3></div>
        @foreach ($recipes as $recipe)
        <a href={{url('user/myRecipes/')}}/{{$recipe->id}}>
            <div class="recipes row">
                <div class="col-md-2 d-none d-md-block">
                    @if ($recipe->image == '')
                        <img src="{{asset('dist/img/download/avocado1_512.png')}}">
                    @else
                        <img src="{{asset('dist/img/download')}}/{{$recipe->image}}">
                    @endif               
                </div>
                <div class="col-md-10 col-12">
                    <h3>{{$recipe->name}}</h3>
                    {{$recipe->description}} 
                </div>
                
            </div>
        </a>
        @endforeach
    </div>
@stop