@extends('admin/templateAdmin')
@section('titre')
    Users
@stop
@section('contenu1')
@parent
    <div class="admin mt-3">
        <div class="row mr-3 mb-3 text-center top-row">
            <div class="col-lg-3 col-4">
                Username
            </div>
            <div class="col-lg-3 col-4">
                Mail
            </div>
            <div class="col-lg-2 col-4">
                Rôle
            </div>
            <div class="col-lg-2 d-none d-lg-block">
                Modifier
            </div>
            <div class="col-lg-2 d-none d-lg-block">
                Supprimer
            </div>
        </div>
        @foreach ($users as $user)
        <div class="row mr-3 mb-3">
            <div class="col-lg-3 col-4">
                {{$user->username}} 
            </div>
            <div class="col-lg-3 col-4">
                {{$user->mail}} 
            </div>
            <div class="col-lg-2 col-4">
                <form action={{url('admin/users/update')}}/{{$user->id}} method="POST" class="p-0 m-0">
                {{Form::select('role', array('user'=>'user', 'admin'=>'admin'), $user->role, ['class' => 'form-control'])}}
            </div>
            <div class="col-lg-2 col-6">
                
                    <button type="submit" class="btn abutton p-0">Modifier</button>
                </form>
            </div>
            <div class="col-lg-2 col-6">
                <form action={{url('admin/users/remove')}}/{{$user->id}} method="POST" class="p-0 m-0">
                    <button type="submit" class="btn abutton p-0">Supprimer</button>
                </form>
            </div>
        </div>
        @endforeach
    </div>
@stop