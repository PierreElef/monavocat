@extends('admin/templateAdmin')
@section('titre')
    Ingredients
@stop
@section('contenu1')
@parent
    <div class="admin mt-3">
        <div class="m-3 text-left">
            <div class="row mr-3 mb-3" style="background-color: transparent">
                <div class="col ">
                    <h3>Liste des ingrédients</h3>
                </div>
                <div class="col">
                    <form action={{url('admin/ingredients/add')}} method="POST" class="p-0 m-0">
                        {{Form::text('name', '', ['class' => 'form-control'])}}
                    </div>
                    <div class="col">
                        <button type="submit" class="btn abutton p-0">Ajouter</button>
                    </form>
                </div>
        </div>
        <div class="row mr-3 mb-3 text-center top-row">
            <div class="col">
                Nom
            </div>
            <div class="col">
                Modifier
            </div>
            <div class="col">
                Supprimer
            </div>
        </div>
        @foreach ($ingredients as $ingredient)
        <div class="row mr-3 mb-3">
            <div class="col">
                <form action={{url('admin/ingredients/update')}}/{{$ingredient->id}} method="POST" class="p-0 m-0">
                {{Form::text('name', $ingredient->name, ['class' => 'form-control'])}}
            </div>
            <div class="col">
                    <button type="submit" class="btn abutton p-0">Modifier</button>
                </form>
            </div>
            <div class="col">
                <form action={{url('admin/ingredients/remove')}}/{{$ingredient->id}} method="POST" class="p-0 m-0">
                    <button type="submit" class="btn abutton p-0">Supprimer</button>
                </form>
            </div>
        </div>
        @endforeach
    </div>
@stop