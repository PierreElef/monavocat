<div class="avonavbar">
    <ul class="nav justify-content-center">
    @foreach (Config::get('navbar.linkAdmin') as $namePage=>$page)
        <li class="nav-item">
            <a class="nav-link" href={{url($page)}}>{{$namePage}}</a>
        </li>
    @endforeach
        <li class="nav-item">
            <a class="nav-link" href={{url('deconnexion')}} alt="Deconnexion">X</a>
        </li>
    </ul>
</div>