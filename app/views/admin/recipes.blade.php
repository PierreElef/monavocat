@extends('admin/templateAdmin')
@section('titre')
    recipes
@stop
@section('contenu1')
@parent
    <div class="admin mt-3">
        
        <div class="row mr-3 mb-3 text-center top-row">
            <div class="col">
                Nom
            </div>
            <div class="col">
                Description
            </div>
            <div class="col">
                Supprimer
            </div>
        </div>
        @foreach ($recipes as $recipe)
        <div class="row mr-3 mb-3">
            <div class="col">
                {{$recipe->name}}
            </div>
            <div class="col">
                {{$recipe->description}}
            </div>
            <div class="col">
                <form action={{url('admin/recipes/remove')}}/{{$recipe->id}} method="POST" class="p-0 m-0">
                    <button type="submit" class="btn abutton p-0">Supprimer</button>
                </form>
            </div>
        </div>
        @endforeach
    </div>
@stop