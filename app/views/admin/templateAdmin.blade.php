<head>
    <meta lang="fr">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{Config::get('navbar.title')}} - @yield('titre')</title>
    <link rel="stylesheet" href=" {{ asset('/dist/css/bootstrap.css') }}" />
    <link rel="stylesheet" href=" {{ asset('/dist/css/style.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
</head>
<html>
    <body class="container backavocado">
        <div class="title">
            <h1>Mon Avocat</h1>
            <h2>Page administrateur</h2>
        </div> 
        @include('admin/navbarAdmin')
        <div class="container">
            @section('contenu1')
            @show
        </div>   
    
        @include('footer')
    </body>
</html>

