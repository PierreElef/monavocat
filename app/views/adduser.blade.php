<head>
    <meta lang="fr">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{Config::get('navbar.title')}} - Créer Compte</title>
    <link rel="stylesheet" href=" {{ asset('/dist/css/bootstrap.css') }}" />
    <link rel="stylesheet" href=" {{ asset('/dist/css/style.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
</head>
<html>
    <body class="adduser container-fluid">
        <div class="title">
            <h1>Mon Avocat</h1>
            <h2>Guide collaboratif des adorateurs d&rsquo;Avocat</h2>
        </div> 
        <div class="container">
            <div class="mb-3">
                Créer votre compte sur MonAvocat pour : 
                    <ul>
                        <li>retrouver toutes les recettes ajoutées par nos adorateurs des avocats.</li>
                        <li>enregistrer vos recettes favorites</li>
                        <li>Rajoutez vous même des recettes</li>
                    </ul>
                Aidez nous à convertir le Monde à l&rsquo;avocat !
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <form action="#" method="POST">
                    <div class="form-group">
                        <label>Login*</label>
                        <input type="text" class="form-control" id="username" name ="username" placeholder="Login" required>
                    </div>
                    <div class="form-group">
                        <label>Mail*</label>
                        <input type="mail" class="form-control" id="mail" name ="mail" placeholder="Mail" required>
                    </div>
                    <div class="form-group">
                        <label>Password*</label>
                        <input type="password" class="form-control" id="password" name ="password" placeholder="Password" required>
                    </div>
                    <button type="submit" class="btn">Créer un compte</button>
                </form>
            </div>
        </div>
    @include('footer')
    </body>
</html>

