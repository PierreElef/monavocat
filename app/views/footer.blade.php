<div>
    <div class="footer text-center">
        <p>Site réalisé par <a href="http://www.pierre-elefterion.fr" target="_blank">Pierre ELEFTERION</a> &copy;<br/>
        Photos by <a href="https://burst.shopify.com/@matthew_henry?utm_campaign=photo_credit&amp;utm_content=Free+Two+Avocado+Slices+Image%3A+Stunning+Photography&amp;utm_medium=referral&amp;utm_source=credit">Matthew Henry</a> from <a href="https://burst.shopify.com/healthy?utm_campaign=photo_credit&amp;utm_content=Free+Two+Avocado+Slices+Image%3A+Stunning+Photography&amp;utm_medium=referral&amp;utm_source=credit">Burst</a><br/>
        Icons made by <a href="https://www.flaticon.com/authors/prettycons" title="prettycons">prettycons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a> and <a href="https://www.pinterest.fr/levineleavitt/">levineleavitt</a></p>
    </div>
</div>