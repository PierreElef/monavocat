<head>
    <meta lang="fr">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{Config::get('navbar.title')}} - Connexion</title>
    <link rel="stylesheet" href=" {{ asset('/dist/css/bootstrap.css') }}" />
    <link rel="stylesheet" href=" {{ asset('/dist/css/style.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
</head>
<html>
    <body class="connexion container-fluid">
        <div class="title">
            <h1>Mon Avocat</h1>
            <h2>Guide collaboratif des adorateurs d&rsquo;Avocat</h2>
        </div> 
        <div class="container">
            <p>Connectez vous pour retrouver toutes les recettes ajoutées par nos adorateurs des avocats.<br/> Enregistrez vos recettes favorites</br>Rajoutez vous même des recettes et aidez nous à convertir le Monde à l&rsquo;avocat</p>
            <div class="col-lg-4 col-md-6 col-12">
                <form action="#" method="POST">
                    <div class="form-group">
                        <label>Login</label>
                        <input type="text" class="form-control" id="username" name ="username" placeholder="Login">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" id="password" name ="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn">Se connecter</button>
                </form>
                <br/>
                <a href={{url('adduser')}} class="center-text">Créer un compte</a> 
            </div>
        </div>
    @include('footer')
    </body>
</html>

