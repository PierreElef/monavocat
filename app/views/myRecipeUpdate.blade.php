@extends('mon_template')
@section('titre')
    Ajouter une recette
@stop
@section('contenu1')
@parent
    <div class="mt-1">
        {{ Form::open(array('method'=>'post', 'files' => true)) }}
        <div class="form-group">
            <label>Nom de la recette</label>
            {{Form::text('name', $recipe->name, ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
                <label>Description</label>
                {{Form::text('description', $recipe->description, ['class' => 'form-control'])}}
            </div>
        <div class="form-group">
            <label>Instructions</label></br>
            {{Form::textarea('instruction', $recipe->instruction,['class' => 'addRecipeTextArea'])}}
        </div>
        {{ Form::submit('Enregistrer', ['class' => 'btn abutton']) }}
        {{ Form::close() }}
        
    </div>
@stop