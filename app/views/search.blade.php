@extends('mon_template')
@section('titre')
    Ajouter une recette
@stop
@section('contenu1')
@parent
    <div class="search mt-3">
        <h3>Rechercher une recette</h3>
        {{ Form::open(array('method'=>'post', 'files' => true)) }}
        <div class="form-group">
            {{ Form::select('searchType', array('nom'=>'nom', 'description'=>'description', 'ingredient'=>'ingredient', 'catégorie'=>'catégorie (entrée, plat, dessert)'), null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{ Form::text('searchInput', '', ['class' => 'form-control'])}}
        </div>
        {{ Form::submit('Rechercher', ['class' => 'btn abutton']) }}
        {{ Form::close() }}
        
    </div>
@stop