<head>
    <meta lang="fr">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{Config::get('navbar.title')}} - Oubli mot de passe</title>
    <link rel="stylesheet" href=" {{ asset('/dist/css/bootstrap.css') }}" />
    <link rel="stylesheet" href=" {{ asset('/dist/css/style.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Lato:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
</head>
<html>
    <body class="container-fluid forget">
        <div class="title">
            <h1>Mon Avocat</h1>
            <h2>Guide collaboratif des adorateurs d&rsquo;Avocat</h2>
        </div> 
        <div class="container">
            <p>Vous avez oublié votre mot de passe ? Commençons par retrouver votre compte</p>
            <div class="col-lg-4 col-md-6 col-12">
                <from  action="#" method="POST">
                    <div class="form-group">
                        <label>Mail</label>
                        <input type="mail" class="form-control" id="mail" name ="mail" placeholder="Mail de votre compte">
                    </div>
                    <button type="submit" class="btn">Submit</button>
                </form>
            </div>
        </div>
    @include('footer')
    </body>
</html>

