@extends('mon_template')
@section('titre')
    Ajouter une recette
@stop
@section('contenu1')
@parent
    <div class="addRecipe mt-1">
        <div class="text-center">Donnez tous les étapes de préparations de votre recette et une image</div>
        <div class="row justify-content-md-center">
            <div class="col-2 step">
                <img src={{ asset('/dist/img/icons/step1.png') }} height="100">
            </div>
            <div class="col-2 step">
                <img src={{ asset('/dist/img/icons/step2.png') }} height="100">
            </div>
            <div class="col-2 step">
                <img src={{ asset('/dist/img/icons/step3.png') }} height="100">
            </div>
        </div>
        
        
        {{ Form::open(array('method'=>'post', 'files' => true)) }}
        @for($i = 0; $i < $session['nbring']; $i++) 
            {{Form::hidden('ing_'.$i, $session['ing_'.$i])}}
            {{Form::hidden('amount_'.$i, $session['amount_'.$i])}}
            {{Form::hidden('meas_'.$i, $session['meas_'.$i])}}
        @endfor
        {{ Form::hidden('name', $session['name']) }}
        {{ Form::hidden('description', $session['description']) }}
        {{ Form::hidden('tag', $session['tag']) }}
        {{ Form::hidden('nbring', $session['nbring']) }}
        <div class="form-group">
            <label>Instructions</label>
            {{Form::textarea('instruction', '',['placeholder'=>"Ecrire les instructions pour la recette", 'class' => 'addRecipeTextArea'])}}
        </div>
        <div class="form-group">
            <label>Image</label><br/>
            {{ Form::file('image') }}
        </div>
        {{ Form::submit('Enregistrer', ['class' => 'btn abutton']) }}
        {{ Form::close() }}
        
    </div>
@stop