@extends('mon_template')
@section('titre')
    Ajouter une recette
@stop
@section('contenu1')
@parent
    <div class="addRecipe mt-1">
        <div class="text-center">Remplissez les infos sur les ingrédients de votre recette.</div>
        <div class="row justify-content-md-center">
            <div class="col-2 step">
                <img src={{ asset('/dist/img/icons/step1.png') }} height="100">
            </div>
            <div class="col-2 step">
                <img src={{ asset('/dist/img/icons/step2.png') }} height="100">
            </div>
            <div class="col-2 step">
                <img src={{ asset('/dist/img/icons/avocado5_512.png') }} height="100">
            </div>
        </div>
        <?php 
            $ingredientArray=[];
            foreach ($ingredients as $ingredient){
                $ingredientArray+=[$ingredient->id => $ingredient->name];
            }
            $measArray=[];
            foreach ($measurements as $measurement){
                $measArray+=[$measurement->id => $measurement->name];
            }
        ?>
        {{ Form::open(array('method'=>'post', 'files' => true)) }}
        @for($i = 0; $i < $session['nbring']; $i++) 
        <div class="form-group">
            <label>Ingredient {{ $i+1 }}</label>
            <div class="row">
                <div class="col-3">
                    {{Form::select('ing_'.$i, $ingredientArray, null, ['class' => 'form-control'])}}
                </div>
                <div class="col-2">
                    <input type="number" name={{"amount_".$i}} min="1" step="1" value="1" class="form-control">
                </div>
                <div class="col-3">
                    {{Form::select('meas_'.$i, $measArray, null, ['class' => 'form-control'])}}
                </div>
            </div>
        </div>
        @endfor
        {{ Form::hidden('name', $session['name']) }}
        {{ Form::hidden('description', $session['description']) }}
        {{ Form::hidden('tag', $session['tag']) }}
        {{ Form::hidden('nbring', $session['nbring']) }}
        {{ Form::submit('Suivant', ['class' => 'btn abutton']) }}
        {{ Form::close() }}
        
    </div>
@stop