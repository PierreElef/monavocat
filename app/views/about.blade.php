@extends('mon_template')
@section('titre')
    A propos
@stop
@section('contenu1')
@parent
    <div style="text-align: justify">
        <h2>L&rsquo;Avocat</h2>
        L&rsquo;avocat est le fruit de l&rsquo;avocatier (Persea americana), un arbre de la famille des Lauraceae, originaire du Mexique. Il en existe trois grandes variétés.
        </br>Le mot « avocat » provient de l&rsquo;espagnol aguacate, lui-même dérivé du mot de langue nahuatl ahuacatl qui signifie « testicule », par analogie à la forme de cet organe.
        </br> C&rsquo;est un fruit climactérique particulier : sa maturation est associée à la production d&rsquo;éthylène.
        </br>Le Mexique est, de loin, le premier producteur mondial de ce fruit (à lui seul 30 % de la production mondiale), ainsi que le premier exportateur et le premier consommateur.
    </div>
    <div style="text-align: justify">
        <h2>Mon Avocat</h2>
        Nous, on aime l&rsquo;avocat et on veut que ça se sache. Nous sommes des adorateurs de l&rsquo;avocat, nous le vénérons, nous le chérrissons et nous lui rendons grâce. Nature, avec de la vinaigrette ou en guacamole, nous mangeons de l&rsquo;avocat.
        </br>Mais on peut se lasser du guacamole. Du coup, on se partage des recettes et on mange encore plus d&rsquo;avocat.
    </div>
    <div style="text-align: justify" class="mb-3">
        <h2>Pourquoi ?</h2>
        Pourquoi manger de l&rsquo;avocat ?
        </br>D&rsquo;abord, parce que c&rsquo;est bon. Ensuite, les médecines traditionnelles l’utilisaient ou l’utilisent encore contre la ménorragie, l’hypertension, les maux d&rsquo;estomac, la bronchite, la diarrhée et le diabète2.
        </br>La consommation d&rsquo;avocats a un effet bénéfique sur le cholestérol sanguin. Une analyse des données de l&rsquo;enquête nationale sur la santé et la nutrition NHANES (en) sur 17 167 adultes aux États-Unis montre que les consommateurs d&rsquo;avocats sont moins exposés au risque de syndrome métabolique que les autres, mais que cette association ne prouve pas la causalité : la consommation d&rsquo;avocats est associée à de meilleures conditions de nutrition et une moindre prévalence de surpoids et d&rsquo;obésité7. Mais une méta-analyse des expérimentations cliniques conduites jusqu&rsquo;en février 2015 démontre les effets bénéfiques de la consommation d&rsquo;avocat8. Par exemple, après sept jours d&rsquo;un régime riche en avocats, des patients en hypercholestérolémie ont vu leur taux de cholestérol total diminuer de 17 %, avec une diminution de 22 % des niveaux de mauvais cholestérol LDL et de triglycérides, et une augmentation de 11 % du bon cholestérol HDL9. Neuf autres études montrent des résultats similaires10.
        </br>Par ailleurs, les glucides qui le composent ne dépassent pas 2 g aux 100 g (une valeur très inférieure à celle des autres fruits frais, où ils atteignent en moyenne 10 g). Il s’agit d’un mélange de glucose, fructose et saccharose, et surtout de substances glucidiques originales, tels des sucres-alcools (comme le perséitol), ou encore des sucres à cinq ou sept atomes de carbone, assez rares dans le règne végétal.
        </br>Les botanistes y ont identifié une peptone, le b-galactoside, de l&rsquo;acide abscissique glycosylé, des alcaloïdes, de la cellulose, la polygalacto uréase, les polyuronoides, le cytochrome P-450 et des huiles volatiles.
    </div>
@stop