@extends('mon_template')
@section('titre')
    Pas de résultat
@stop
@section('contenu1')
@parent
    <div class="center-text">
            <h2>Pas de résultat. <a href={{url("user/search")}}>Recommencer ?</a></h2>
            <p align="center"><img src="{{asset('dist/img/icons/errorResults.PNG')}}"></p>
    </div>
@stop