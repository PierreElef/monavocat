<?php
return array(
    'title'=>'Mon Avocat',
    'link'=> [
        'Recettes'=>'recipes',
        'Mes recettes' => 'user/myRecipes',
        'Ajouter une recette' => 'user/recipes/add/step1',
        'Rechercher' => 'user/search',
        'A propos' => 'about',
        'Profil' => 'user/profil',
    ],
    'linkAdmin'=> [
        'Utilisateurs'=>'admin/users',
        'Ingredients' => 'admin/ingredients',
        'Recettes' => 'admin/recipes',
        'Revenir en Normal' => 'recipes',
    ]
);