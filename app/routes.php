<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Zone public*/
Route::get('/', function(){
	return Redirect::to('recipes');
});

/*Homepage -> Recipes*/
Route::get('recipes', function(){
	$recipes = Recipe::all();
	return View::make('recipes')->with('recipes', $recipes);
});

Route::group(array('prefix' => 'recipes'), function(){
	Route::get('', function($nom = null) {
		$recipes = Recipe::all();
		return View::make('recipes')->with('recipes', $recipes);
	});
	Route::get('/{id}', function ($id, $nom = null) {
		$recipe = Recipe::find($id);
		$recipe_ing_meas = Recipe_ing_meas::where('id_recipe', '=', $id)->get();
		$recipe_tag = Recipe_tag::where('id_recipe', '=', $id)->first();
		$tag_id = $recipe_tag->id_tag;
		$tag= Tag::where('id', '=', $tag_id)->first();
		$recipe_ing_measArray=[];
		$i=0;
		foreach($recipe_ing_meas as $recipeIngMea){
			$ingredient=Ingredient::where('id', '=', $recipeIngMea->id_ingredient)->first();
			$recipe_ing_measArray+=["ing_".$i => $ingredient->name];
			$measurement=Measurement::where('id', '=', $recipeIngMea->id_measurement)->first();
			$recipe_ing_measArray+=["meas_".$i => $measurement->name];
			$recipe_ing_measArray+=["amount_".$i => $recipeIngMea->amount];
			$i=$i+1;
		}
		$recipe_ing_measArray+=["size"=>$i];
		return View::make('recipe')->with(array('recipe'=>$recipe, 'recipe_ing_measArray'=>$recipe_ing_measArray, 'tag'=>$tag));
	});
});

Route::get('about', function(){
	return View::make('about');
});

/*Se connecter*/
Route::get('connexion', function(){
	return View::make('connexion');
});
Route::post('connexion', function(){
	$username = Input::get('username');
    $password = Input::get('password');
 
    if(Auth::attempt(array('username' => $username, 'password' => $password)))
		return Redirect::to('recipes');
    else
		return Redirect::to('connexion');
});
/*Rajouter un utilisateur*/
Route::get('adduser', function(){
	return View::make('adduser');
});
Route::post('adduser', function(){
	$data=Input::all();
	$user = new User;
	$user->username = $data['username'];
    $user->mail = $data['mail'];
    $user->password = $data['password'];
	$user->role = 'user';
	$user->save();
	return Redirect::to('connexion');
});
/*Oubli de mot de passe*/
Route::get('forget', function(){
	return View::make('forget');
});
Route::post('forget', function(){
	$mail=Input::all();
	$user = User::find($id);
	$user->password = Hash::make('avokánto1234');
	return Redirect::to('connexion');
});

//Zone Réservée User
Route::group(array('prefix' => 'user'), function(){
	Route::group(array('prefix' => '/myRecipes'), function(){
		Route::get('',array('before' => 'auth', function() {
			$recipes = Recipe::where('id_user', '=', Auth::user()->id)->get();
			return View::make('myRecipes')->with('recipes', $recipes);
		}));
		Route::get('/{id}', array('before' => 'auth', function ($id) {
			$recipe = Recipe::find($id);
			$recipe_ing_meas = Recipe_ing_meas::where('id_recipe', '=', $id)->get();
			$recipe_tag = Recipe_tag::where('id_recipe', '=', $id)->first();
			$tag_id = $recipe_tag->id_tag;
			$tag= Tag::where('id', '=', $tag_id)->first();
			$recipe_ing_measArray=[];
			$i=0;
			foreach($recipe_ing_meas as $recipeIngMea){
				$ingredient=Ingredient::where('id', '=', $recipeIngMea->id_ingredient)->first();
				$recipe_ing_measArray+=["ing_".$i => $ingredient->name];
				$measurement=Measurement::where('id', '=', $recipeIngMea->id_measurement)->first();
				$recipe_ing_measArray+=["meas_".$i => $measurement->name];
				$recipe_ing_measArray+=["amount_".$i => $recipeIngMea->amount];
				$i=$i+1;
			}
			$recipe_ing_measArray+=["size"=>$i];
			return View::make('myRecipe')->with(array('recipe'=>$recipe, 'recipe_ing_measArray'=>$recipe_ing_measArray, 'tag'=>$tag));
		}));
		Route::get('/{id}/edit', array('before' => 'auth', function ($id) {
			$recipe = Recipe::find($id);
			return View::make('myRecipeUpdate')->with('recipe', $recipe);
		}));
		Route::post('/{id}/edit', array('before' => 'auth', function ($id) {
			$recipe = Recipe::find($id);
			$data=Input::all();
			$recipe->name = $data['name'];
			$recipe->description = $data['description'];
			$recipe->instruction =  $data['instruction'];
			$recipe->save();
			return Redirect::to('user/myRecipes/'.$id);
			
		}));
	});

	Route::get('/recipes/add/step1', array('before' => 'auth', function(){
		$tags=Tag::all();
		return View::make('addRecipeStep1')->with('tags', $tags);
	}));

	Route::post('/recipes/add/step1', array('before' => 'auth', function(){
		$data=Input::all();
		return Redirect::to('user/recipes/add/step2')->with('data', $data);
	}));

	Route::get('/recipes/add/step2', array('before' => 'auth', function(){
		$session=Session::get('data');
		$ingredients=Ingredient::all();
		$measurements=Measurement::all();
		return View::make('addRecipeStep2')->with(array('session'=>$session, 'ingredients'=>$ingredients, 'measurements'=>$measurements));
	}));

	Route::post('/recipes/add/step2', array('before' => 'auth', function(){
		$data=Input::all();
		return Redirect::to('user/recipes/add/step3')->with('data', $data);
	}));

	Route::get('/recipes/add/step3', array('before' => 'auth', function(){
		$session=Session::get('data');
		return View::make('addRecipeStep3')->with('session', $session);
	}));

	Route::post('/recipes/add/step3', 'RecipeController@Create');

	Route::get('/profil', array('before' => 'auth', function(){
		$user=User::find(Auth::user()->id);
		return View::make('profil')->with('user', $user);
	}));

	Route::post('/profil', array('before' => 'auth', function () {
		$data=Input::all();
		$user=User::find(Auth::user()->id);
		$user->username = $data['username'];
		$user->mail = $data['mail'];
		$user->password = Hash::make($data['password']);
		$user->save();
		return Redirect::to('recipes');
	}));

	Route::get('/search', array('before' => 'auth', function(){
		return View::make('search');
	}));

	Route::post('/search', array('before' => 'auth', function () {
		$data=Input::all();
		switch ($data['searchType']){
			case 'description':
				$recipes = Recipe::where('description', 'like', '%'.$data['searchInput'].'%')->first();
			break;
			case 'ingredient':
				$ingredient = Ingredient::where('name', 'like', '%'.$data['searchInput'].'%')->first();
				if(empty($ingredient)){
					return View::make('errorResult');
				}else{
					$recipe_ing_meas = Recipe_ing_meas::where('id_ingredient', '=', $ingredient->id)->get();
					$recipeArray=[];
					$i=0;
					foreach ($recipe_ing_meas as $key => $value) {
						$recipe = Recipe::where('id', 'like', $value['id_recipe'])->first();
						$recipeArray+=["id_".$i => $recipe->id];
						$recipeArray+=["name_".$i => $recipe->name];
						$recipeArray+=["description_".$i => $recipe->description];
						$recipeArray+=["image_".$i => $recipe->image];
						$i=$i+1;
					}	
					$recipeArray+=["size"=>$i];
				}
				if(count($recipeArray)>1){
					return View::make('results')->with('recipes', $recipeArray);
				}else{
					return View::make('errorResult');
				}
			break;
			case 'nom':
				$recipes = Recipe::where('name', 'like', '%'.$data['searchInput'].'%')->get();
			break;
			case 'catégorie':
				$tag=Tag::where('name', 'like', '%'.$data['searchInput'].'%')->first();
				if(empty($tag)){
					return View::make('errorResult');
				}else{
					$recipe_tags=Recipe_tag::where('id_tag', '=', $tag->id)->get();
					$recipeArray=[];
					$i=0;
					foreach ($recipe_tags as $key => $value) {
						$recipe = Recipe::where('id', 'like', $value['id_recipe'])->first();
						$recipeArray+=["id_".$i => $recipe->id];
						$recipeArray+=["name_".$i => $recipe->name];
						$recipeArray+=["description_".$i => $recipe->description];
						$recipeArray+=["image_".$i => $recipe->image];
						$i=$i+1;
					}	
					$recipeArray+=["size"=>$i];
				}
				if(count($recipeArray)>1){
					return View::make('results')->with('recipes', $recipeArray);
				}else{
					return View::make('errorResult');
				}
			break;
		}
		
		if(count($recipes)>0){
			return View::make('recipes')->with('recipes', $recipes);
		}else{
			return View::make('errorResult');
		}
		
	}));
	Route::get('/errorResult', array('before' => 'auth', function(){
		return View::make('errorResult');
	}));
});

Route::get('deconnexion', function(){
	Auth::logout();
	return Redirect::to('connexion');
});


/*ADMIN*/
Route::group(array('prefix' => 'admin'), function(){
	Route::get('', array('before' => 'admin', function($nom = null) {
		return Redirect::to('admin/users');
	}));
	/*USERS*/
	/*READ*/
	Route::get('/users', array('before' => 'admin', function () {
		$users=User::all();
		return View::make('admin/users')->with('users', $users);
	}));
	/*UPDATE*/
    Route::post('/users/update/{id}', array('before' => 'admin', function ($id) {
        $user=User::find($id);
        $data=Input::all();
		$user->role = $data['role'];
		$user->save();
		return Redirect::to('admin/users');
    }));
    /*DELETE*/
    Route::post('/users/remove/{id}', array('before' => 'admin', function ($id) {
        $user=User::find($id);
        $user->delete();
		return Redirect::to('admin/users');
    }));
	/*INGREDIENTS*/
	/*READ*/
	Route::get('/ingredients', array('before' => 'admin', function () {
		$ingredients=Ingredient::all();
		return View::make('admin/ingredients')->with('ingredients', $ingredients);
	}));
	/*CREATE*/
	Route::post('/ingredients/add', array('before' => 'admin', function () {
        $data=Input::all();
		$ingredient=new Ingredient;
		$ingredient->name=$data['name'];
		$ingredient->save();
		return Redirect::to('admin/ingredients');
	}));
	/*UPDATE*/
    Route::post('/ingredients/update/{id}', array('before' => 'admin', function ($id) {
        $ingredient=Ingredient::find($id);
        $data=Input::all();
		$ingredient->name = $data['name'];
		$ingredient->save();
		return Redirect::to('admin/ingredients');
	}));
	/*DELETE*/
	Route::post('/ingredients/remove/{id}', array('before' => 'admin', function ($id) {
        $ingredient=Ingredient::find($id);
        $ingredient->delete();
		return Redirect::to('admin/ingredients');
    }));
	/*RECETTES*/
	/*READ*/
	Route::get('/recipes', array('before' => 'admin', function () {
		$recipes = Recipe::all();
		return View::make('admin/recipes')->with('recipes', $recipes);
	}));
	/*DELETE*/
	Route::post('/recipes/remove/{id}', array('before' => 'admin', function ($id) {
        $recipe=Recipe::find($id);
        $recipe->delete();
		return Redirect::to('admin/recipes');
    }));
});