<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipes extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::create('recipes', function(Blueprint $table){
			$table->increments('id');
			$table->string('name', 100);
			$table->string('description', 1000);
			$table->string('instruction', 2000);
			$table->string('image', 255);
			$table->integer('id_user');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down(){
		Schema::drop('recipes');
	}

}
