<?php
class MeasurementsTableSeeder extends Seeder {
    public function run() {
        DB::table('measurements')->insert(
            array(
                array(
                    'name' => '--',
                ),
                array(
                    'name' => 'mg',
                ),
                array(
                    'name' => 'g',
                ),
                array(
                    'name' => 'kg',
                ),
                array(
                    'name' => 'pincée(s)',
                ),
                array(
                    'name' => 'cs',
                ),
                array(
                    'name' => 'cc',
                ),
                array(
                    'name' => 'mL',
                ),
                array(
                    'name' => 'cL',
                ),
                array(
                    'name' => 'L',
                ),

            )
        );
    }
}