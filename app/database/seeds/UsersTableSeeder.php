<?php
class UsersTableSeeder extends Seeder {
    public function run() {
        DB::table('users')->insert(
            array(
                array(
                    'username' => 'PierreE',
                    'mail' => 'pierre.elefterion@ynov.com',
                    'password' => Hash::make('pierre'),
                    'role' => 'admin',
                ),
 
                array(
                    'username' => 'JoffreyJ',
                    'mail' => 'joffrey.jeuhomme.@ynov.com',
                    'password' => Hash::make('joffrey'),
                    'role' => 'user',
                ),
            )
        );
    }
}
