<?php
class Recipe_tagTableSeeder extends Seeder {
    public function run() {
        DB::table('recipe_tag')->insert(
            array(
                array(
                    'id_recipe' => '1',
                    'id_tag' => '1'
                ),
                array(
                    'id_recipe' => '2',
                    'id_tag' => '3'
                ),
            )
        );
    }
}