<?php
class TagsTableSeeder extends Seeder {
    public function run() {
        DB::table('tags')->insert(
            array(
                array(
                    'name' => 'entrée',
                ),
                array(
                    'name' => 'plat',
                ),
                array(
                    'name' => 'dessert',
                ),
            )
        );
    }
}