<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
    {
		$this->call('RecipesTableSeeder');
		$this->call('UsersTableSeeder');
		$this->call('TagsTableSeeder');
		$this->call('IngredientsTableSeeder');
		$this->call('MeasurementsTableSeeder');
		$this->call('Recipe_ing_measTableSeeder');
    }


}
