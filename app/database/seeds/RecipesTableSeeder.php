<?php
class RecipesTableSeeder extends Seeder {
    public function run() {
        DB::table('recipes')->insert(
            array(
                array(
                    'name' => 'Avocat au sel',
                    'description' => "Entrée la plus simple au monde à condition d'avoir un avocat et du sel",
                    "instruction" => "Couper l'avocat en 2,\r\nenlever le noyau,\r\nmettre du sel",
                    'image' => '',
                    'id_user' => '1',
                ),
 
                array(
                    'name' => "Cheesecake à l'avocat",
                    'description' => "Gateau à l'avocat et au citron",
                    'instruction' => "ÉTAPE 1\r\nPrélevez le zeste des 4 citrons verts et râpez-le.\r\n\r\nÉTAPE 2\r\nCoupez les citrons verts en deux et pressez-les afin de récupérer leur jus.\r\n\r\nÉTAPE 3\r\nVersez le jus de citron dans une casserole.\r\n\r\nÉTAPE 4\r\nAjoutez-y les zestes râpés, 80 g de sucre en poudre et la maïzena. Mélangez.\r\n\r\nÉTAPE 5\r\nPlacez la casserole sur feu doux et laissez chauffer jusqu\'à épaississement.\r\n\r\nÉTAPE 6\r\nRetirez dès lors du feu et laissez refroidir.\r\n\r\nÉTAPE 7\r\nRéduisez les biscuits au chocolat en poudre dans un mixeur.\r\n\r\nÉTAPE 8\r\nMélangez la poudre de biscuits avec le beurre fondu dans un saladier jusqu\'à obtenir une pâte sableuse.\r\n\r\nÉTAPE 9\r\nRépartissez la pâte sableuse dans le fond d\'un cercle à pâtisserie recouvert de papier sulfurisé. Tassez bien la surface avec vos mains. Réservez au frais.\r\n\r\nÉTAPE 10\r\nPréchauffez le four à 140°C.\r\n\r\nÉTAPE 11\r\nPrélevez la chair des avocats et mixez-la dans un robot mixeur jusqu\'à obtenir une purée bien lisse et homogène.\r\n\r\nÉTAPE 12\r\nMélangez la purée d\'avocats avec le mascarpone dans un saladier.\r\n\r\nÉTAPE 13\r\nAjoutez le fromage Philadelphia et le restant de sucre en poudre. Fouettez bien.\r\n\r\nÉTAPE 14\r\nAjoutez les oeufs un par un puis la moitié du jus de citron vert épaissi. Remuez bien.\r\n\r\nÉTAPE 15\r\nVersez le mélange sur la pâte sableuse dans le cercle à pâtisserie. Lissez bien la surface avec une spatule.\r\n\r\nÉTAPE 16\r\nEnfournez pendant 45 minutes.\r\n\r\nÉTAPE 17\r\nA la fin de la cuisson, éteignez le four et laissez refroidir le cheesecake dans le four éteint pendant 1 heure.\r\n\r\nÉTAPE 18\r\nPlacez-le ensuite au frais pendant 12 heures minimum avant le service.\r\n\r\nÉTAPE 19\r\nAu moment de servir, démoulez délicatement le cheesecake et nappez-le du restant de jus de citron épaissi.\r\n\r\nÉTAPE 20\r\nDécorez le cheesecake comme bon vous semble et servez-le bien frais.",
                    'image' => 'i40916-cheesecake-a-l-avocat-et-au-citron-vert.jpg',
                    'id_user' => '2',
                ),
            )
        );
    }
}