<?php
class Recipe_ing_measTableSeeder extends Seeder {
    public function run() {
        DB::table('recipe_ing_meas')->insert(
            array(
                array(
                    'id_recipe' => '1',
                    'id_ingredient' => '1',
                    'id_measurement' => '1',
                    'amount' => '1',
                ),
                array(
                    'id_recipe' => '1',
                    'id_ingredient' => '45',
                    'id_measurement' => '5',
                    'amount' => '3',
                ),
                array(
                    'id_recipe' => '2',
                    'id_ingredient' => '1',
                    'id_measurement' => '1',
                    'amount' => '2',
                ),
                array(
                    'id_recipe' => '2',
                    'id_ingredient' => '2',
                    'id_measurement' => '3',
                    'amount' => '50',
                ),
                array(
                    'id_recipe' => '2',
                    'id_ingredient' => '11',
                    'id_measurement' => '1',
                    'amount' => '4',
                ),
                array(
                    'id_recipe' => '2',
                    'id_ingredient' => '17',
                    'id_measurement' => '3',
                    'amount' => '400',
                ),
                array(
                    'id_recipe' => '2',
                    'id_ingredient' => '30',
                    'id_measurement' => '3',
                    'amount' => '100',
                ),
                array(
                    'id_recipe' => '2',
                    'id_ingredient' => '46',
                    'id_measurement' => '3',
                    'amount' => '150',
                ),
                array(
                    'id_recipe' => '2',
                    'id_ingredient' => '34',
                    'id_measurement' => '1',
                    'amount' => '3',
                ),
                array(
                    'id_recipe' => '2',
                    'id_ingredient' => '29',
                    'id_measurement' => '1',
                    'amount' => '1',
                ),
            )
        );
        DB::table('recipe_tags')->insert(
            array(
                array(
                    'id_recipe' => '1',
                    'id_tag' => '1'
                ),
                array(
                    'id_recipe' => '2',
                    'id_tag' => '3'
                ),
            )
        );
    }
}