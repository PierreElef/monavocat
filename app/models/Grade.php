<?php
class Grade extends Eloquent {
    
    public function user(){
        return $this->belongsTo('User');
    }
    
    public function recipe(){
        return $this->belongsTo('Recipe');
    }
    
}
    
?>