<?php
class Recipe extends Eloquent {

    public function user(){
        return $this->belongsTo('User');
    }

    public function grades(){
        return $this->belongsToMany('Grade');
    }

    public function tags(){
        return $this->belongsToMany('Tag');
    }
}
?>