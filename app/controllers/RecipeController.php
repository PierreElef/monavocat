<?php

class RecipeController extends BaseController {
    public function Create() {
		$recipe = new Recipe;
		$data=Input::all();
		$recipe->name = $data['name'];
		$recipe->description = $data['description'];
		$recipe->instruction =  $data['instruction'];
		$recipe->id_user = Auth::user()->id;
		$name=$data['image']->getClientOriginalName();
		if(isset($name)){
			$imageName=date('dmyHis').'-'.$data['image']->getClientOriginalName();
			$recipe->image = $imageName;
			$data['image']->move(base_path() . '/public/dist/img/download/', $imageName);
		}else{
			$recipe->image = '';
		}
		$recipe->save();

		for($i = 0; $i < $data['nbring']; $i++){
			$recipe_ing_meas = new Recipe_ing_meas;
			$recipe_ing_meas->id_recipe=$recipe->id;
			$recipe_ing_meas->id_ingredient=$data['ing_'.$i];
			$recipe_ing_meas->id_measurement=$data['meas_'.$i];
			$recipe_ing_meas->amount=$data['amount_'.$i];
			$recipe_ing_meas->save();
		}

		$recipe_tag = new Recipe_tag;
		$recipe_tag->id_recipe=$recipe->id;
		$recipe_tag->id_tag=$data['tag'];
		$recipe_tag->save();

		return Redirect::to('recipes');
	}

}